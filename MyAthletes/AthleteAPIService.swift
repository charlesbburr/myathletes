//
//  AthleteAPIService.swift
//  MyAthletes
//
//  Class used to formulate API requets, only accessed from the repository layer 
//  Created by Charles Burr on 31/12/2021.
//

import Foundation

enum MyAthletesAPIError: Error {
  case failedRequest
  case noData
  case invalidResponse
  case invalidData
}

final class AthleteAPIService {
  
  typealias AthleteAPICompletion = ((Any?, MyAthletesAPIError?) -> Void)
  
  private let baseURLString: String
  private static let urlSession = URLSession.shared
  
  init(baseURLString: String) {
    self.baseURLString = baseURLString
  }
  
  func getAll(completion: @escaping AthleteAPICompletion) {
    
    // Mock server request
    let randomReturnTime = Int.random(in: 0..<1000)
    DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + .milliseconds(randomReturnTime), execute: {
      let athlete1 = AthleteModel(uuid: UUID(), firstName: "John", lastName: "Doe", dob: Date().addingTimeInterval(-24*60*60*365*15), imageURL: "https://res.cloudinary.com/thecornerapp-com/image/upload/v1641245576/harvey-gibson-ThqaTYD_l0o-unsplash.jpg")
      let athlete2 = AthleteModel(uuid: UUID(), firstName: "Paul", lastName: "Evans", dob: Date().addingTimeInterval(-24*60*60*365*20.4), imageURL: "https://res.cloudinary.com/thecornerapp-com/image/upload/v1641245983/justus-menke-s2OisyY9YGU-unsplash.jpg")
      let athlete3 = AthleteModel(uuid: UUID(), firstName: "Lauren", lastName: "Witherspoon", dob: Date().addingTimeInterval(-24*60*60*365*25.1), imageURL: nil)
      let athlete4 = AthleteModel(uuid: UUID(), firstName: "Laura", lastName: "Moon", dob: Date().addingTimeInterval(-24*60*60*365*28.2), imageURL: "https://res.cloudinary.com/thecornerapp-com/image/upload/v1641245576/gabin-vallet-qq_wRiGXf7o-unsplash.jpg")
      let athlete5 = AthleteModel(uuid: UUID(), firstName: "Pete", lastName: "Larry", dob: Date().addingTimeInterval(-24*60*60*365*31.6), imageURL: "https://res.cloudinary.com/thecornerapp-com/image/upload/v1641245576/elijah-hiett-vAVGdV1oklQ-unsplash.jpg")
      let athlete6 = AthleteModel(uuid: UUID(), firstName: "Jane", lastName: "Smith", dob: Date().addingTimeInterval(-24*60*60*365*34.7), imageURL: nil)
      let athlete7 = AthleteModel(uuid: UUID(), firstName: "Bridget", lastName: "Price", dob: Date().addingTimeInterval(-24*60*60*365*21.3), imageURL: nil)
      let athletes = [athlete1, athlete2, athlete3, athlete4, athlete5, athlete6, athlete7]
      completion(athletes, nil)
    })
  
  }
  
  func update(_ athlete: AthleteModel, completion: @escaping AthleteAPICompletion) {
    
    // Mock server request
    let randomReturnTime = Int.random(in: 0..<300)
    DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + .milliseconds(randomReturnTime), execute: {
      completion(athlete, nil)
    })
    
  }
  
  func create(firstName: String, lastName: String, dob: Date, completion: @escaping AthleteAPICompletion) {
    
    // Mock server request
    let randomReturnTime = Int.random(in: 0..<300)
    DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + .milliseconds(randomReturnTime), execute: {
      let athlete = AthleteModel(uuid: UUID(), firstName: firstName, lastName: lastName, dob: dob)
      completion(athlete, nil)
    })
    
  }
  
  func getImage(forAthlete athlete: AthleteModel) {
    
  }
  
  
}
