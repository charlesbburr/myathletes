//
//  UIColor.swift
//  MyAthletes
//
//  Created by Charles Burr on 31/12/2021.
//

import Foundation
import UIKit.UIColor

//MARK - Provide definitions for the app's colors

extension UIColor {
  
  static func MABackgroundColor() -> UIColor {
    return UIColor(red: 17/255, green: 24/255, blue: 36/255, alpha: 1.0)
  }
  
  static func MANavigiationBarColor() -> UIColor {
    return UIColor.MABackgroundColor().mix(with: UIColor.white, amount: 0.05)
  }
  
  static func MAContainerBackgroundColor() -> UIColor {
    return UIColor.MABackgroundColor().mix(with: UIColor.white, amount: 0.04)
  }
  
  static func MABaseTextColor() -> UIColor {
    return UIColor.white
  }
  
  static func MAHighlightColor() -> UIColor {
    return UIColor.white.withAlphaComponent(0.05)
  }
  
  static func MAPrimaryColor() -> UIColor {
    return UIColor(red: 172/255, green: 253/255, blue: 223/255, alpha: 1.0)
  }
  
  static func MABorderColor() -> UIColor {
    return UIColor.white.mix(with: UIColor.MABackgroundColor(), amount: 0.92)
  }
  
  static func MAMediumBorderColor() -> UIColor {
    return UIColor.white.mix(with: UIColor.MABackgroundColor(), amount: 0.8)
  }
  
  static func MAPlaceholderColor() -> UIColor {
    return UIColor.white.mix(with: UIColor.MABackgroundColor(), amount: 0.4)
  }
  
}

// Helper functions

extension UIColor {
    
  func mix(with color: UIColor, amount: CGFloat) -> Self {
    
    var red1: CGFloat = 0
    var green1: CGFloat = 0
    var blue1: CGFloat = 0
    var alpha1: CGFloat = 0
    var red2: CGFloat = 0
    var green2: CGFloat = 0
    var blue2: CGFloat = 0
    var alpha2: CGFloat = 0

    getRed(&red1, green: &green1, blue: &blue1, alpha: &alpha1)
    color.getRed(&red2, green: &green2, blue: &blue2, alpha: &alpha2)

    return Self(
      red: red1 * CGFloat(1.0 - amount) + red2 * amount,
      green: green1 * CGFloat(1.0 - amount) + green2 * amount,
      blue: blue1 * CGFloat(1.0 - amount) + blue2 * amount,
      alpha: alpha1
    )
  }

  func lighter(by amount: CGFloat = 0.2) -> Self {
    mix(with: .white, amount: amount)
  }

  func darker(by amount: CGFloat = 0.2) -> Self {
    mix(with: .black, amount: amount)
  }
  
}

extension UIColor {

  /// Converts this `UIColor` instance to a 1x1 `UIImage` instance and returns it.
  ///
  /// - Returns: `self` as a 1x1 `UIImage`.
  func as1ptImage() -> UIImage {
    UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
    setFill()
    UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
    let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    UIGraphicsEndImageContext()
    return image
  }
}
