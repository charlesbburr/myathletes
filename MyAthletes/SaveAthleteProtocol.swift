//
//  SaveAthleteProtocol.swift
//  MyAthletes
//
//  Used to expose creating/update athlete ability
//  Created by Charles Burr on 03/01/2022.
//

import Foundation

protocol SaveAthleteProtocol {
  
  func update(
    uuid: UUID,
    firstName: String,
    lastName: String,
    dob: Date,
    imageURL: String?,
    completion: @escaping AthleteRepository.AthleteRepositoryCompletion)
  
  func create(
    firstName: String,
    lastName: String,
    dob: Date,
    imageURL: String?,
    completion: @escaping AthleteRepository.AthleteRepositoryCompletion)
  
}
