//
//  Box.swift
//  MyAthletes
//
//  Simple class used to wrap any value, to allow a listener to bind and receive value updates.
//  Created by Charles Burr on 03/01/2022.
//

import Foundation

final class Box<T> {
  
  typealias Listener = (T) -> Void
  var listener: Listener?
  
  var value: T {
    didSet {
      listener?(value)
    }
  }
  
  init(_ value: T) {
    self.value = value
  }
  
  func bind(listener: Listener?) {
    self.listener = listener
    listener?(value)
  }
  
}
