//
//  ImageManager.swift
//  MyAthletes
//
//  Singleton class to manage cache for images.
//  Created by Charles Burr on 03/01/2022.
//

import Foundation
import UIKit

final class ImageManager {
  
  static var shared = ImageManager()
  private let cache: NSCache<NSString,UIImage> = NSCache()
  private var taskCache = [String:URLSessionDataTask]()
  
  private init() {}
  
}

//MARK: - Shared

extension ImageManager {
  
  private func addImage(_ image: UIImage, forKey key: String) {
    cache.setObject(image, forKey: key as NSString)
  }
  
  private func getImage(forKey key: String) -> UIImage? {
    return cache.object(forKey: key as NSString)
  }
  
}

//MARK: - Internal

extension ImageManager {
  
  func fetchRequest(with input: URL, completion: @escaping (UIImage?, MyAthletesAPIError?) -> ()) {
    let key = input.absoluteString
    
    // Check if we've already cached the image.
    if let cachedImage = getImage(forKey: key) {
      completion(cachedImage, nil)
      return
    }
    
    // Fetch the image.
    let task = URLSession.shared.dataTask(with: input) { [unowned self] (data, response, error) in
  
      if let _ = error {
        completion(nil, .failedRequest)
        return
      }

      guard let data = data else {
        completion(nil, .noData)
        return
      }
      
      guard let image = UIImage(data: data) else {
        completion(nil, .invalidData)
        return
      }
      
      completion(image, nil)
      self.addImage(image, forKey: key)
      
    }
  
    task.resume()
  }
  
}
