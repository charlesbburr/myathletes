//
//  AthleteRepository.swift
//  MyAthletes
//
//  Used to manage athlete data, and abstract the method of storage to the viewcontroller/view layer. 
//  Created by Charles Burr on 31/12/2021.
//

import Foundation

final class AthleteRepository {
  
  typealias AthleteRepositoryCompletion = ((Bool, MyAthletesAPIError?) -> Void)
  private let currentAthletes = Box([AthleteModel]())
  private let service: AthleteAPIService
  
  init(service: AthleteAPIService) {
    
    self.service = service
    
  }
  
}

//MARK: - Read methods

extension AthleteRepository {
  
  func bind(listener: @escaping (([AthleteModel]) -> ())) {
    currentAthletes.bind(listener: listener)
  }
  
  func getAll(completion: @escaping AthleteRepositoryCompletion) {
    
    service.getAll() {
      [weak self] results, error in
      
      guard let self = self else { return }
      DispatchQueue.main.async {
        guard error == nil,
              let athletes = results as? [AthleteModel]
        else {
          completion(false, error)
          return
        }
        // Update repo
        self.currentAthletes.value = athletes
        completion(true, nil)
      }
    }
    
  }
  
}

//MARK: - Save Athlete Protocol
// Used to expose creating/update athlete ability to viewcontroller layer

extension AthleteRepository: SaveAthleteProtocol {
  
  func update(uuid: UUID, firstName: String, lastName: String, dob: Date, imageURL: String?, completion: @escaping AthleteRepositoryCompletion) {
    
    let athlete = AthleteModel(uuid: uuid, firstName: firstName, lastName: lastName, dob: dob, imageURL: imageURL)
    
    service.update(athlete) {
      [weak self] result, error in
      
      guard let self = self else { return }
      DispatchQueue.main.async {
        guard error == nil,
              let athlete = result as? AthleteModel
        else {
          completion(false, error)
          return
        }
        guard let index = self.currentAthletes.value.firstIndex(where: {$0.uuid == uuid }) else {
          completion(false, error)
          return
        }
        // Update repo
        self.currentAthletes.value[index] = athlete
        completion(true, nil)
      }
      
    }
    
  }
 
  func create(firstName: String, lastName: String, dob: Date, imageURL: String?, completion: @escaping AthleteRepositoryCompletion) {
    
    service.create(firstName: firstName, lastName: lastName, dob: dob) {
      [weak self] result, error in
      
      guard let self = self else { return }
      DispatchQueue.main.async {
        guard error == nil,
              let athlete = result as? AthleteModel
        else {
          completion(false, error)
          return
        }
        // Update repo
        self.currentAthletes.value.append(athlete)
        completion(true, nil)
        
      }
      
    }
    
  }
  
}

