//
//  AthleteModel.swift
//  MyAthletes
//
//  Created by Charles Burr on 31/12/2021.
//

import Foundation
import UIKit.UIImage

public struct AthleteModel {
  
  let uuid: UUID
  let firstName: String
  let lastName: String
  let dob: Date
  var imageURL: String?
  
  var fullName: String {
    return firstName + " " + lastName
  }
  
  var age: Int {
    
    let now = Date()
    let birthday: Date = dob
    let calendar = Calendar.current

    let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
    return ageComponents.year!
    
  }
  
}
