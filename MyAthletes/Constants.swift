//
//  Constants.swift
//  MyAthletes
//
//  Created by Charles Burr on 31/12/2021.
//

import Foundation
import UIKit

struct Constants {
  
  static let scaleFactor: CGFloat = {
    
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
      return 414 / 320.0
    } else {
      let width: CGFloat
      if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
        width = UIScreen.main.bounds.height
      } else {
        width = UIScreen.main.bounds.width
      }
      return width / 320.0
    }
    
  }()
  
  static let outerMargin: CGFloat = {
    
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
      let width: CGFloat
      if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
        width = UIScreen.main.bounds.height
      } else {
        width = UIScreen.main.bounds.width
      }
      return width/20
    } else {
      return 10*scaleFactor
    }
    
  }()
  
  static let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    return dateFormatter
  }()
  
  static let dateOnlyFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "MMM d, yyyy"
    return dateFormatter
  }()
}
