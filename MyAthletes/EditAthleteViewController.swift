//
//  EditAthleteViewController.swift
//  MyAthletes
//
//  View controller used to update/create athletes. Has write access to the repository. 
//  Created by Charles Burr on 31/12/2021.
//

import UIKit

enum AthleteFieldType: CaseIterable {
  case firstName
  case lastName
  case dob
  
  var fieldTitle: String {
    switch self {
    case .firstName:
      return "First Name"
    case .lastName:
      return "Last Name"
    case .dob:
      return "Date of Birth"
    }
  }
  
  var keyboardType: UIKeyboardType {
    switch self {
    default:
      return .alphabet
    }
  }
  
  var pickerType: PickerType? {
    switch self {
    case .dob:
      return .date
    default:
      return nil
    }
  }
  
}

enum PickerType {
  case date
}

final class EditAthleteViewController: UIViewController {

  private let isNew: Bool
  private let saveAthleteHandler: SaveAthleteProtocol
  private var athlete: AthleteModel?
  
  private var stackView: UIStackView!
  private var textFields: [AthleteFieldType: UITextField]!
  private let types: [AthleteFieldType] = [.firstName, .lastName, .dob]
  
  private lazy var datePicker: UIDatePicker = {
    let datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    if #available(iOS 13.4, *) {
      datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
    }
    datePicker.datePickerMode = .date
    return datePicker
  }()
  
  init(athlete: AthleteModel?, saveAthleteHandler: SaveAthleteProtocol) {
    
    if let athlete = athlete {
      self.athlete = athlete
      self.isNew = false
    } else {
      self.isNew = true
    }
    self.saveAthleteHandler = saveAthleteHandler
    super.init(nibName: nil, bundle: nil)
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    hideKeyboardWhenTappedAround()
    
    setAppearance()
    setupNavigationBar()
    setupStackView()
    setupTextFields()
    if let athlete = athlete {
      populateFields(forAthlete: athlete)
    }
    
  }

}

//MARK: - Setup UI

extension EditAthleteViewController {
  
  private func setAppearance() {
    
    view.backgroundColor = .MABackgroundColor()
    
  }
  
  private func setupNavigationBar() {
    
    // For all view controllers in navigation controller (only when this view controller is root)
    if let navigationController =  navigationController,
       navigationController.viewControllers.count == 1
       {
      let navigationBar = navigationController.navigationBar
      navigationBar.isTranslucent = true
      navigationBar.titleTextAttributes = [.foregroundColor: UIColor.MABaseTextColor()]
      navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.MABaseTextColor()]
      navigationBar.tintColor = .MAPrimaryColor()
      navigationBar.barTintColor = .MABackgroundColor()
    }
    
    // For this view controller
    if isNew {
      title = NSLocalizedString("Create", comment: "Title for VC when creating a new athlete")
      navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(closeButtonAction))
      navigationItem.hidesBackButton = true
    } else {
      title = NSLocalizedString("Edit", comment: "Title for VC when editing an athlete")
      navigationItem.hidesBackButton = false
    }
    setupDoneButton()
    navigationItem.largeTitleDisplayMode = .never
    
  }
  
  @objc private func closeButtonAction() {
    
    dismiss(animated: true, completion: nil)
    
  }
  
  private func setupDoneButton() {
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButtonAction))
    navigationItem.rightBarButtonItem?.isEnabled = false
    
  }
  
  @objc private func doneButtonAction() {
    
    guard
      let textFields = textFields,
      let firstName = textFields[.firstName]?.text,
      let lastName = textFields[.lastName]?.text,
      let dobString = textFields[.dob]?.text,
      let dob = Constants.dateOnlyFormatter.date(from: dobString)
    else { return }
    
    showLoadingAnimation()
    view.isUserInteractionEnabled = false
    
    if isNew {
      saveAthleteHandler.create(
        firstName: firstName,
        lastName: lastName,
        dob: dob,
        imageURL: nil
      ) { [weak self] success, error in
        self?.hideLoadingAnimation()
        if success {
          self?.dismiss(animated: true, completion: nil)
        } else {
          self?.view.isUserInteractionEnabled = false
        }
      }
    } else if let uuid = athlete?.uuid {
      saveAthleteHandler.update(
        uuid: uuid,
        firstName: firstName,
        lastName: lastName,
        dob: dob,
        imageURL: athlete?.imageURL
      ) { [weak self] success, error in
        self?.hideLoadingAnimation()
        if success {
          self?.navigationController?.popViewController(animated: true)
        } else {
          self?.view.isUserInteractionEnabled = false
        }
      }
    }
    
  }
  
  private func setupStackView() {
    
    stackView = UIStackView()
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.alignment = .fill
    stackView.axis = .vertical
    stackView.layer.cornerRadius = 7*Constants.scaleFactor
    stackView.backgroundColor = .MAContainerBackgroundColor()
    stackView.isLayoutMarginsRelativeArrangement = true
    stackView.layoutMargins = UIEdgeInsets(top: 0, left: 10*Constants.scaleFactor, bottom: 0, right: 0)
    stackView.isLayoutMarginsRelativeArrangement = true
    view.addSubview(stackView)
    
    guard let stackView = stackView else { return }
    view.addConstraint(NSLayoutConstraint(item: stackView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .topMargin, multiplier: 1.0, constant: 15*Constants.scaleFactor))
    view.addConstraint(NSLayoutConstraint(item: stackView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leadingMargin, multiplier: 1.0, constant: 0))
    view.addConstraint(NSLayoutConstraint(item: stackView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailingMargin, multiplier: 1.0, constant: 0))
    
  }
  
  private func setupTextFields() {
    
    guard let stackView = stackView else { return }
    textFields = [:]
    
    for (index,type) in types.enumerated() {
      
      let textField = UITextField()
      textField.translatesAutoresizingMaskIntoConstraints = false
      textField.attributedPlaceholder = NSAttributedString(
        string: type.fieldTitle,
        attributes: [
          .font: UIFont.systemFont(ofSize: 14*Constants.scaleFactor),
          .foregroundColor: UIColor.MABaseTextColor().withAlphaComponent(0.6)
        ])
      textField.keyboardType = type.keyboardType
      textField.font = UIFont.systemFont(ofSize: 14*Constants.scaleFactor)
      textField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
      textField.textColor = .MABaseTextColor()
      textField.returnKeyType = .done
      textField.delegate = self
      stackView.addArrangedSubview(textField)
      textFields[type] = textField
      
      if index < types.count - 1 {
        let div = UIView()
        div.translatesAutoresizingMaskIntoConstraints = false
        div.backgroundColor = UIColor.MABackgroundColor()
        stackView.addArrangedSubview(div)
        stackView.addConstraint(NSLayoutConstraint(item: div, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 1*Constants.scaleFactor))
      }
      
      if type == .dob {
        
        // Setup date picker for dob.
        // This would need to be adapted if there were mulitple fields that required a date input or picker
        let now = Date()
        datePicker.maximumDate = now
        if let dob = athlete?.dob {
          datePicker.date = dob
        } else {
          datePicker.date = now.addingTimeInterval(-24*60*60*365*15)
        }
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(datePickerDidSave))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let titleButton = UIBarButtonItem(title: textField.placeholder, style: .plain, target: nil, action: nil)
        titleButton.isEnabled = false
        titleButton.setTitleTextAttributes([.foregroundColor : UIColor.black], for: .disabled)
        toolbar.setItems([titleButton,spaceButton,doneButton], animated: false)
        textField.tintColor = .clear
        textField.inputAccessoryView = toolbar
        textField.inputView = datePicker
        
      }
      stackView.addConstraint(NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 43*Constants.scaleFactor))
    
    }

  }
  
  @objc private func textFieldDidChange(sender: UITextField) {
    
    updateDoneButton()
    
  }
  
  private func updateDoneButton() {
    
    var shouldEnableButton = false
    if areAllFieldsValid() {
      if isNew && areAllFieldsValid() {
        shouldEnableButton = true
      } else if !isNew && isThereAnyChange() && areAllFieldsValid() {
        shouldEnableButton = true
      }
    }
    if shouldEnableButton {
      enableDoneButton()
    } else {
      disableDoneButton()
    }
    
  }
  
  private func showLoadingAnimation() {
    
    let activityIndicator = UIActivityIndicatorView(style: .medium)
    activityIndicator.color = .white
    activityIndicator.hidesWhenStopped = true
    activityIndicator.startAnimating()
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    
  }
  
  private func hideLoadingAnimation() {
    
    setupDoneButton()
    
  }
  
  private func disableDoneButton() {
    
    navigationItem.rightBarButtonItem?.isEnabled = false
    
  }
  
  private func enableDoneButton() {
    
    navigationItem.rightBarButtonItem?.isEnabled = true
  
  }
  
  @objc private func datePickerDidSave() {
    
    guard let dateTextField = textFields?[.dob] else { return }
    dateTextField.text = Constants.dateOnlyFormatter.string(from: datePicker.date)
    textFieldDidChange(sender: dateTextField)
    view.endEditing(true)
    
  }
  
  
}

extension EditAthleteViewController {
  
  private func populateFields(forAthlete athlete: AthleteModel) {
    
    guard let textFields = textFields else { return }
    textFields[.firstName]?.text = athlete.firstName
    textFields[.lastName]?.text = athlete.lastName
    textFields[.dob]?.text = Constants.dateOnlyFormatter.string(from: athlete.dob)
    
  }
  
  private func isThereAnyChange() -> Bool {
    
    guard
      let athlete = athlete,
      let textFields = textFields
    else { return false }
    
    if athlete.firstName != textFields[.firstName]?.text { return true }
    if athlete.lastName != textFields[.lastName]?.text { return true }
    if Constants.dateOnlyFormatter.string(from: athlete.dob) != textFields[.dob]?.text { return true }
    return false
    
  }
  
  private func areAllFieldsValid() -> Bool {
    
    if textFields.count == 0 { return false }
    for (_, textField) in textFields {
      guard let text = textField.text, text.count > 0 else {
        return false
      }
    }
    return true
    
  }
  
}

extension EditAthleteViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    view.endEditing(true)
    return true
    
  }
  
}
