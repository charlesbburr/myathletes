//
//  AppDelegate.swift
//  MyAthletes
//
//  Created by Charles Burr on 31/12/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
   
    // Override point for customization after application launch.
    
    self.window = UIWindow(frame: UIScreen.main.bounds)
    
    let apiService = AthleteAPIService(baseURLString: "")
    let repository = AthleteRepository(service: apiService)
    self.window?.rootViewController = UINavigationController(rootViewController: AthleteViewController(repository: repository))
    self.window?.makeKeyAndVisible()
    
    return true
    
  }


}

