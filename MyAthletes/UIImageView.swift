//
//  UIImageView.swift
//  MyAthletes
//
//  Created by Charles Burr on 03/01/2022.
//

import UIKit

//MARK - Helper functions

extension UIImageView {
  
  // Works with ImageManager to load images from remote/cache
  
  func setImage(with URL: URL) {
    ImageManager.shared.fetchRequest(with: URL) { [weak self] (image, error) in
      guard
        let self = self,
        error == nil
      else { return }
      DispatchQueue.main.async {
          self.image = image
      }
    }
  }
  
}
