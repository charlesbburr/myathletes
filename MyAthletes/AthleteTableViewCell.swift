//
//  AthleteTableViewCell.swift
//  MyAthletes
//
//  Created by Charles Burr on 31/12/2021.
//

import UIKit

class AthleteTableViewCell: UITableViewCell {

  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var ageLabel: UILabel!
  @IBOutlet weak var underlineView: UIView!
  
  @IBOutlet weak var imageViewLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var avatarImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var avatarImageViewBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var nameLabelToAvatarImageViewXConstraint: NSLayoutConstraint!
  @IBOutlet weak var nameLabelTrailingConstraint: NSLayoutConstraint!
  @IBOutlet weak var ageLabelBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var ageLabelToNameLabelYConstraint: NSLayoutConstraint!
  @IBOutlet weak var underlineViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var underlineViewLeadingConstraint: NSLayoutConstraint!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    setConstraints()
    setAppearance()
  }
  
  private func setAppearance() {
    
    backgroundColor = .clear
    contentView.backgroundColor = .clear
    
    avatarImageView.contentMode = .scaleAspectFill
    avatarImageView.layer.borderWidth = 1 * Constants.scaleFactor
    avatarImageView.layer.borderColor = UIColor.MAMediumBorderColor().cgColor
    avatarImageView.tintColor = UIColor.white
    
    nameLabel.font = .systemFont(ofSize: 16*Constants.scaleFactor)
    nameLabel.textColor = .MABaseTextColor()
    
    ageLabel.font = .systemFont(ofSize: 14*Constants.scaleFactor)
    ageLabel.textColor = .MABaseTextColor().withAlphaComponent(0.8)
    
    underlineView.backgroundColor = .clear // .MABorderColor()
    
  }
  
  private func setConstraints() {
    
    // Adjusts constraints to the device view port
    let topMargin = 3 * Constants.scaleFactor
    let bottomMargin = 5 * Constants.scaleFactor
    imageViewTopConstraint?.constant = topMargin
    imageViewLeadingConstraint?.constant = 0
    avatarImageViewBottomConstraint?.constant = bottomMargin
    let height = 40*Constants.scaleFactor
    avatarImageViewHeightConstraint?.constant = height
    avatarImageView?.layer.cornerRadius = height/2
    
    nameLabelToAvatarImageViewXConstraint?.constant = 14*Constants.scaleFactor
    nameLabelTrailingConstraint?.constant = 0 // 20*Constants.scaleFactor
    
    ageLabelBottomConstraint?.constant = bottomMargin
    ageLabelToNameLabelYConstraint?.constant = 5*Constants.scaleFactor
    
    underlineViewHeightConstraint?.constant = 1*Constants.scaleFactor
    underlineViewLeadingConstraint?.constant = 0
    
  }
  
  func shortenUnderlineView() {
    
    revealUnderlineView()
    guard let imageViewLeading = imageViewLeadingConstraint?.constant,
          let imageViewWidth = avatarImageViewHeightConstraint?.constant,
          let nameLeading = nameLabelToAvatarImageViewXConstraint?.constant
    else { return }
    underlineViewLeadingConstraint?.constant = imageViewLeading + imageViewWidth + nameLeading
    
  }
  
  func showFullUnderlineView() {
    
    revealUnderlineView()
    underlineViewLeadingConstraint?.constant = 0
    
  }
  
  func hideUnderlineView() {
    
    underlineView?.isHidden = true
    
  }
  
  func revealUnderlineView() {
    
    underlineView?.isHidden = false
    
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }
  
  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    super.setHighlighted(highlighted, animated: animated)
    
    if highlighted {
      highlight()
    } else {
      unhighlight()
    }
    
  }
  
  private func highlight() {
    
    backgroundColor = .MAHighlightColor()
    
  }
  
  private func unhighlight() {
    
    backgroundColor = .clear
    
  }
    
}


