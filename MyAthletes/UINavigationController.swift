//
//  UINavigationController.swift
//  MyAthletes
//
//  Created by Charles Burr on 02/01/2022.
//

import UIKit.UINavigationController

extension UINavigationController {

  func setNavigationBarBorderColor(_ color: UIColor) {
    self.navigationBar.shadowImage = color.as1ptImage()
  }
}
