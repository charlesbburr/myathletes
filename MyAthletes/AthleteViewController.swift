//
//  ViewController.swift
//  MyAthletes
//
//  Main view controller to show the list of athletes. This is the only view controller that has read/write access to the repository.
//  Created by Charles Burr on 31/12/2021.
//

import UIKit

final class AthleteViewController: UITableViewController {

  private let repository: AthleteRepository
  private var retrievedAthletes: [AthleteModel] = []
  
  init(repository: AthleteRepository) {
    self.repository = repository
    super.init(style: .plain)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setAppearance()
    setupNavigationBar()
    setupTableView()
    
    repository.bind(listener: { [weak self] athletes in
      guard let self = self
      else { return }
      DispatchQueue.main.async {
        self.retrievedAthletes = athletes
        self.tableView?.reloadData()
      }
    })
    requestAthletes()
    
  }
  
  private func requestAthletes() {
    
    // Load data
    repository.getAll() {
      [weak self] success, error in
      // TODO if not sucess, show error to user and option to retry.
      // however, as we are using mock data, we know it will succeed
    }
    
  }

}

//MARK: - Setup UI

extension AthleteViewController {
  
  private func setupTableView() {
    
    guard let tableView = tableView else { return }
    tableView.register(UINib(nibName: "AthleteTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "AthleteTableViewCell")
    tableView.allowsMultipleSelection = false
    tableView.allowsSelection = true
    tableView.dataSource = self
    tableView.estimatedRowHeight = 60*Constants.scaleFactor
    tableView.separatorStyle = .none
    
  }
  
  private func setAppearance() {
    
    view.backgroundColor = .MABackgroundColor()
    
  }
  
  private func setupNavigationBar() {
    
    // For all view controllers in navigation controller
    if let navigationController = navigationController {
      let navigationBar = navigationController.navigationBar
      navigationBar.prefersLargeTitles = true
      navigationBar.isTranslucent = true
      navigationBar.titleTextAttributes = [.foregroundColor: UIColor.MABaseTextColor()]
      navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.MABaseTextColor()]
      navigationBar.tintColor = .MAPrimaryColor()
      navigationBar.barTintColor = .MABackgroundColor()
    }
    
    // For this view controller
    title = NSLocalizedString("Athletes", comment: "Title for top of Athletes View Controller")
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(addButtonAction))
    
  }
  
  @objc private func addButtonAction() {
    
    let vc = EditAthleteViewController(athlete: nil, saveAthleteHandler: repository)
    present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    
  }
  
}

//MARK: - UITableViewDelegate

extension AthleteViewController {
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    guard
      indexPath.row < retrievedAthletes.count
    else {
      return
    }
    let athlete = retrievedAthletes[indexPath.row]
    let vc = EditAthleteViewController(athlete: athlete, saveAthleteHandler: repository)
    navigationController?.pushViewController(vc, animated: true)
    
  }
  
}

//MARK: - UITableViewDataSource

extension AthleteViewController {
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return retrievedAthletes.count
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard
      let cell = tableView.dequeueReusableCell(withIdentifier: "AthleteTableViewCell") as? AthleteTableViewCell,
      indexPath.row < retrievedAthletes.count
    else {
      return UITableViewCell()
    }
  
    let athlete = retrievedAthletes[indexPath.row]
    cell.nameLabel?.text = athlete.fullName
    cell.ageLabel?.text = "\(athlete.age)"
    cell.avatarImageView?.image = UIImage(named: "AvatarIcon")?.withTintColor(.MAPlaceholderColor(), renderingMode: .alwaysTemplate)
    if let imageURLString = athlete.imageURL,
       let imageURL = URL(string: imageURLString) {
      cell.avatarImageView?.setImage(with: imageURL)
    }
    if indexPath.row == retrievedAthletes.count - 1 {
      //cell.showFullUnderlineView()
      cell.hideUnderlineView()
    } else {
      cell.shortenUnderlineView()
    }
    
    return cell
    
  }
  
}
