# MyAthletes

A simple application to show a list of athletes with the option to add and edit them.


## Architecture

This app follows the MVC architecture with separated layers; each layer only talking to the layer above or below.

Layers are as follows:
View
ViewController: Sets up views, updated by the repository through binding.
Repository: Manage data, and abstract the method of storage to the ViewController layer.
Service: Build and handle network requests (the API is a mock in this example)

A global singleton class "ImageManager" is used to load/cache images across the app.

## Notes

1.  The network calls are faked in this example, with hardcoded values being returned from the service layer. This includes name, date of birth and optional image URL.
2.  Images are being returned from a remote URL for a few of the hardcoded users.
3.  Placeholder avatar image (shown when loading or no images is available for user) is compiled as an asset in the app.
